package com.bootcamp.nttdata.preferencias

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.bootcamp.nttdata.basic.util.Activity
import com.bootcamp.nttdata.basic.util.intentTo


class BlankFragment : Fragment() {

    lateinit var btn :Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    /**
     * Este es un comentario
     *
     *
     */

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_blank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn = requireActivity().findViewById(R.id.btnReturn)
        btn.setOnClickListener{
            val intent: Intent = intentTo(Activity.Main, requireContext())!!
            startActivity(intent)
        }
    }

}