package com.bootcamp.nttdata.tab

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.bootcamp.nttdata.basic.util.Activity
import com.bootcamp.nttdata.basic.util.intentTo
import com.bootcamp.nttdata.tab.ui.main.SectionsPagerAdapter
import com.bootcamp.nttdata.tab.databinding.ActivityTabBinding

class TabActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTabBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTabBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = binding.viewPager
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = binding.tabs
        tabs.setupWithViewPager(viewPager)
        val fab: FloatingActionButton = binding.fab

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Vamos al menu principal", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            val intent =  intentTo(Activity.Main, this)
            startActivity(intent)
        }
    }
}