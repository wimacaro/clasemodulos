package com.bootcamp.nttdata.basic.util

import android.content.Context
import android.content.Intent

private const val PACKAGE_NAME = "com.bootcamp.nttdata"

fun intentTo(intentActivity: IntentActivity, context : Context): Intent? =
    Intent(Intent.ACTION_VIEW).setClassName(context,intentActivity.className)

interface IntentActivity {
    /**
     * The activity class name.
     */
    val className: String
}

object Activity {
    /**
     * DetailActivity
     */
    object Preferences : IntentActivity {
        override val className = "$PACKAGE_NAME.preferencias.PreferencesActivity"
    }
    object Main : IntentActivity {
        override val className = "$PACKAGE_NAME.basic.MainActivity"
    }
    object Tab : IntentActivity {
        override val className = "$PACKAGE_NAME.tab.TabActivity"
    }

}